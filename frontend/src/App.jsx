import React from 'react';
import Home from './pages/Home/Home.jsx';
import { Context } from './Context';



function App() {
  return (
    <div>
      <Context >
        <Home />  
      </Context>
    </div>
  );
}

export default App;
