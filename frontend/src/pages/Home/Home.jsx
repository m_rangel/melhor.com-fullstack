import React from "react";
import Drawer from "../../components/Drawer";
import AppBar from "../../components/AppBar";
import { ListPhones } from "../../components/ListPhones";
import { CountBar } from "../../components/CountBar";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import { Paper, Snackbar, IconButton } from "@material-ui/core";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import { RegisterDialog } from "../../components/RegisterPhoneDialog";
import Close from "@material-ui/icons/Close";


const drawerWidth = 240;
const useStyles = makeStyles(theme => ({
  fab: {
    position: "absolute",
    bottom: theme.spacing(2),
    right: theme.spacing(2)
  },

  root: {
    display: "flex"
  },
  toolbar: {
    paddingRight: 24 // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: "0 8px",
    ...theme.mixins.toolbar
  },
  appBar: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.black.white,
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginRight: 36
  },
  menuButtonHidden: {
    display: "none"
  },
  title: {
    flexGrow: 1
  },
  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    width: "0px",
    [theme.breakpoints.up("sm")]: {
      width: "0px"
    }
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto"
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4)
  },
  paper: {
    padding: theme.spacing(0),
    display: "flex",
    overflow: "auto",
    flexDirection: "column"
  },
  fixedHeight: {
    height: 240
  },

  mainMargin: {
    margin: "2%"
  },

  chipsMargin: {
    margin: "2% 0 2% 0"
  }
}));

export default () => {
  const [openSnack, setOpenSnak] = React.useState({status:false, message: ""});

  const classes = useStyles();
  const [open, setOpen] = React.useState(true);
  const [openRegister, setOpenRegister] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleClickRegister = () => {
    setOpenRegister(true);
  };

  const handleCloseRegister = () => {
    setOpenRegister(false);
  };

  const handleSucessRegister = (data) =>{
    setOpenSnak({status:true, message: "Criado com sucesso"});
    handleCloseRegister();
  }

  const handleErrorRegister = (data) =>{
    setOpenSnak({status:true, message: data.error + data.message});
  }


  function handleClose(event, reason) {
    if (reason === 'clickaway') {
      return;
    }

    setOpenSnak({status:false, message: ""});
  }

  return (
    <div className={classes.root}>
      <CssBaseline />

      <Drawer
        open={open}
        handleDrawerClose={handleDrawerClose}
        classes={classes}
      />

      <AppBar
        open={open}
        handleDrawerOpen={handleDrawerOpen}
        classes={classes}
      />
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />

        <div className={classes.mainMargin}>
          <div className={classes.chipsMargin}>
            <CountBar />
          </div>

          <Paper>
            <ListPhones />
          </Paper>
        </div>

        <Fab
          color="primary"
          aria-label="Add"
          className={classes.fab}
          onClick={handleClickRegister}
        >
          <AddIcon />
        </Fab>

        <RegisterDialog
          open={openRegister}
          handleClose={handleCloseRegister}
          handleSucess={handleSucessRegister}
          handleError={handleErrorRegister}
        />
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center"
          }}
          open={openSnack.status}
          autoHideDuration={6000}
          onClose={handleClose}
          ContentProps={{
            "aria-describedby": "message-id"
          }}
          message={<span id="message-id">{openSnack.message}</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              onClick={handleClose}
            >
              <Close />
            </IconButton>
          ]}
        />
      </main>
    </div>
  );
};
