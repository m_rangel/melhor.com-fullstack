import React, { useContext } from 'react';
import { PhoneTotalContext } from '../Context';
import {  Chip } from '@material-ui/core';
import Smartphone from '@material-ui/icons/Smartphone';


export const CountBar = () => { 


    const [count] = useContext(PhoneTotalContext);

    return (
        <div>

            <Chip icon={<Smartphone />} label={'Total phones found: ' + count}  />
           
        </div>
    );

}