import React, { useContext, useState, useEffect } from "react";
import Moment from "react-moment";

/////Table
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

////Avatar
import Avatar from "@material-ui/core/Avatar";
import missingImage from "../assets/img/missingImage.svg";

import Edit from "@material-ui/icons/Edit";
import Delete from "@material-ui/icons/Delete";
import Close from "@material-ui/icons/Close";

import IconButton from "@material-ui/core/IconButton";

//// API
import { create } from "apisauce";

import {
  QueryPhoneContext,
  PhoneTotalContext,
  PhoneResultContext
} from "../Context";

import { CurrencyReal } from "./CurrencyReal";
import {
  withStyles,
  TableFooter,
  TablePagination,
  Snackbar,
  Button
} from "@material-ui/core";
import { UpdateDialog } from "./UpdatePhoneDialog";

const api = create({
  baseURL: "http://localhost:4000/"
});

const StyledTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white
  },
  body: {
    fontSize: 14
  }
}))(TableCell);

const StyledTableRow = withStyles(theme => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.background.default
    }
  }
}))(TableRow);

export const ListPhones = () => {

  const [openUpdate, setOpenUpdate] = React.useState(false);
  const [updateRow, setUpdateRow] = React.useState({});
  
  const [openSnack, setOpenSnak] = React.useState({status:false, message: ""});
  const [page, setPage] = useState(0);
  const [result, setResult] = useContext(PhoneResultContext);
  const [query, setQuery] = useContext(QueryPhoneContext);
  const [count, setCount] = useContext(PhoneTotalContext);

  useEffect(() => {
    api.get("phones", query).then(response => {
      setCount(response.data.count);
      setResult(response.data.phones);
    });
  }, Object.values(query));

  function handleChangePage(event, newPage) {
    query.page = parseInt(newPage + 1, 10);
    setQuery({ ...query });
    setPage(newPage);
  }

  function startEditing(row) {
    setUpdateRow(row);
    setOpenUpdate(true)
  }

  const handleCloseRegister = () => {
    setOpenUpdate(false);
  };

  const handleSucessRegister = (data) =>{
    debugger;
    setOpenSnak({status:true, message: "Atualizado com sucesso"});
    handleCloseRegister();
  }

  const handleErrorRegister = (data) =>{
    setOpenSnak({status:true, message: data.error + data.message});
  }

  function handleClose(event, reason) {
    if (reason === 'clickaway') {
      return;
    }

    setOpenSnak({status:false, message: ""});
  }

  function handleRemove(row) {

    const url = `phones/${row.code}`;
    
    api.delete(url).then(response => {
      debugger;
      if(response.status === 200){
        setOpenSnak({status:true, message: "Successfully deleted"});
        setPage(page);
      }else{
        setOpenSnak({status:true, message: response.data.error + ": " +response.data.message});
      }
      
    });
  }

  return (
    <div>
      <Table>
        <TableHead>
          <StyledTableRow>
            <StyledTableCell align="left">Photo</StyledTableCell>
            <StyledTableCell align="left">Code</StyledTableCell>
            <StyledTableCell align="left">Model</StyledTableCell>
            <StyledTableCell align="left">Brand</StyledTableCell>
            <StyledTableCell align="left">Price</StyledTableCell>
            <StyledTableCell align="left">Start Date</StyledTableCell>
            <StyledTableCell align="left">End Date</StyledTableCell>
            <StyledTableCell align="left">Colour</StyledTableCell>
            <StyledTableCell align="left">Edit</StyledTableCell>
            <StyledTableCell align="left">Delete</StyledTableCell>
          </StyledTableRow>
        </TableHead>
        <TableBody>
          {result.map(row => (
            <StyledTableRow key={row.id}>
              <StyledTableCell align="left">
                <Avatar
                  src={row.photo}
                  imgProps={{
                    onError: e => {
                      e.target.src = missingImage;
                    }
                  }}
                />
              </StyledTableCell>
              <StyledTableCell align="left">{row.code}</StyledTableCell>
              <StyledTableCell align="left">{row.model}</StyledTableCell>
              <StyledTableCell align="left">{row.brand}</StyledTableCell>
              <StyledTableCell align="left">
                <CurrencyReal value={row.price} />
              </StyledTableCell>
              <StyledTableCell align="left">
                <Moment format="DD/MM/YYYY">{row.startDate}</Moment>
              </StyledTableCell>
              <StyledTableCell align="left">
                <Moment format="DD/MM/YYYY">{row.endDate}</Moment>
              </StyledTableCell>
              <StyledTableCell align="left">{row.colour}</StyledTableCell>
              <StyledTableCell>
                <IconButton aria-label="Edit" onClick={() => startEditing(row)}>
                  <Edit  />
                </IconButton>
              </StyledTableCell>
              <StyledTableCell>
                <IconButton aria-label="Delete" onClick={() => handleRemove(row)}>
                  <Delete  />
                </IconButton>
              </StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[5]}
              colSpan={8}
              count={count}
              rowsPerPage={5}
              page={page}
              SelectProps={{
                inputProps: { "aria-label": "Rows per page" },
                native: true
              }}
              onChangePage={handleChangePage}
            />
          </TableRow>
        </TableFooter>
      </Table>
      <UpdateDialog
          row={updateRow}
          open={openUpdate}
          handleClose={handleCloseRegister}
          handleSucess={handleSucessRegister}
          handleError={handleErrorRegister}
        />
      <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          open={openSnack.status}
          autoHideDuration={6000}
          onClose={handleClose}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">{openSnack.message}</span>}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              onClick={handleClose}
            >
              <Close />
            </IconButton>,
        ]}
      />
    </div>
  );
};
