import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  makeStyles
} from "@material-ui/core";

//// API
import { create } from "apisauce";

const api = create({
    baseURL: "http://localhost:4000/"
  });

const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  }
}));

export const RegisterDialog = ({ open, handleClose, handleSucess, handleError }) => {
  const classes = useStyles();
  const defaultDate = ["2018-12-25", "2025-12-30"];

  const [brand, setBrand] = useState("");
  const [model, setModel] = useState("");
  const [code, setCode] = useState("");

  const [price, setPrice] = useState(0);
  const [photo, setPhoto] = useState("");

  const [startDate, setStartDate] = useState(defaultDate[0]);
  const [endDate, setEndDate] = useState(defaultDate[1]);

  const [colour, setColour] = useState("BLACK");


  const handleBrandChange = event => {
    setBrand(event.target.value);
  };

  const handleModelChange = event => {
  setModel(event.target.value);
};
  const handleCodeChange = event => {
    setCode(event.target.value);
  };
  const handlePriceChange = event => {
    setPrice(event.target.value);
  };
  const handlePhotoChange = event => {
    setPhoto( event.target.value );
  };
  const handleStartDateChange = event => {
    setStartDate( event.target.value );
  };
  const handleEndDateChange = event => {
    setEndDate( event.target.value );
  };

  const handleColourChange = event => {
    setColour( event.target.value );
  };

  const handleClear = () =>{

    setBrand("");
    setModel("");
    setCode("");
    setColour("BLACK");
    setPhoto("");
    setPrice(0);
    setEndDate(defaultDate[1]);
    setStartDate(defaultDate[0]);

  }

  const handleSubmit = () => {

    let startDateFix = new Date(new Date(startDate).setDate(new Date(startDate).getDate() + 1));
    let endDateFix = new Date(new Date(endDate).setDate(new Date(endDate).getDate() + 1));

    let data = {
        "code" : code,
        "model" : model,
        "price" : parseFloat( price , 10 ),
        "brand" : brand,
        "photo" : photo,
        "startDate" : startDateFix ,
        "endDate" : endDateFix ,
        "colour" : colour
    }


    api.post("phones", data).then(response => {
        if(response.status === 201){
            handleSucess(response.data);
            handleClear();
        }
        else{
            console.log(response.data);
            handleError(response.data);
        }

    });

  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Phone Register</DialogTitle>
        <DialogContent>
          <TextField
            required
            label="Code"
            value={code}
            onChange={handleCodeChange}
            margin="normal"
            variant="outlined"
            fullWidth
          />
          <TextField
            required
            label="Model"
            value={model}
            onChange={handleModelChange}
            margin="normal"
            variant="outlined"
            fullWidth
          />
          <TextField
            required
            label="Price"
            value={price}
            onChange={handlePriceChange}
            margin="normal"
            variant="outlined"
            fullWidth
          />
          <TextField
            required
            label="Brand"
            value={brand}
            onChange={handleBrandChange}
            margin="normal"
            variant="outlined"
            fullWidth
          />
          <TextField
            required
            label="Photo"
            value={photo}
            onChange={handlePhotoChange}
            margin="normal"
            variant="outlined"
            fullWidth
          />
          <TextField
            required
            label="Start Date"
            margin="normal"
            type="date"
            value={startDate}
            onChange={handleStartDateChange}
            variant="outlined"
            fullWidth
          />
          <TextField
            required
            label="End Date"
            margin="normal"
            type="date"
            value={endDate}
            onChange={handleEndDateChange}
            variant="outlined"
            fullWidth
          />

          <FormControl
            required
            variant="outlined"
            className={classes.formControl}
            fullWidth
          >
            <InputLabel htmlFor="colour">Colour</InputLabel>
            <Select 
                name="colour" 
                margin="normal" 
                variant="outlined" 
                value={colour}
                onChange={handleColourChange}
                fullWidth>
              <MenuItem value="WHITE">WHITE</MenuItem>
              <MenuItem value="BLACK">BLACK</MenuItem>
              <MenuItem value="GOLD">GOLD</MenuItem>
              <MenuItem value="PINK">PINK</MenuItem>
            </Select>
          </FormControl>
        </DialogContent>

        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleSubmit} color="primary">
            Register
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
