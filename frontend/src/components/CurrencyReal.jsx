import numeral from 'numeral'
import React from 'react';

numeral.register('locale', 'pt-BR', {
    delimiters: {
        thousands: '.',
        decimal: ','
    },
    currency: {
        symbol: 'R$ '
    }
});

numeral.locale('pt-BR');

export const CurrencyReal = ({value}) => {

    return (
        <div>
            {numeral(value).format( '$0,0.00')}
        </div>
    );

}