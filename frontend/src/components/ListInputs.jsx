import React, { useContext, useState } from "react";
import Slider from "@material-ui/lab/Slider";
import DeleteIcon from "@material-ui/icons/Delete";
import Button from "@material-ui/core/Button";
import {
  IconButton,
  makeStyles,
  MenuItem,
  InputLabel,
  FormControl,
  Typography
} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import { queryParams, QueryPhoneContext } from "../Context";
import clsx from "clsx";

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));

const defaultValues = queryParams;

let controlChange = {
  code: false,
  model: false,
  brand: false,
  price: false,
  startDate: false,
  endDate: false,
  colour: false
};

export const ListInput = () => {
  const classes = useStyles();
  const defaultPrice = [0, 10000];
  const defaultDate = ["2018-12-25", "2025-12-30"];
  const [query, setQuery] = useContext(QueryPhoneContext);

  const [codeValue, setCodeValue] = useState("");
  const [modelValue, setModelValue] = useState("");
  const [brandValue, setBrandValue] = useState("");
  const [priceValue, setPriceValue] = useState(defaultPrice);
  const [maxStartDateValue, setMaxStartDateValue] = useState(defaultDate[1]);
  const [minStartDateValue, setMinStartDateValue] = useState(defaultDate[0]);
  const [maxEndDateValue, setMaxEndDateValue] = useState(defaultDate[1]);
  const [minEndDateValue, setMinEndDateValue] = useState(defaultDate[0]);
  const [colourValue, setColourValue] = useState("");

  const handleConfirm = () => {
    if (controlChange.price) {
      if (priceValue[0] < priceValue[1]) {
        query.minPrice = priceValue[0];
        query.maxPrice = priceValue[1];
      } else {
        query.minPrice = priceValue[1];
        query.maxPrice = priceValue[0];
      }

      if (query.maxPrice >= 10000) {
        query.maxPrice = 99999;
      }
    }

    if (controlChange.startDate) {
      query.minStartDate = minStartDateValue;
      query.maxStartDate = maxStartDateValue;
    }

    if (controlChange.endDate) {
        query.minEndDate = minEndDateValue;
        query.maxEndDate = maxEndDateValue;
      }

    if (controlChange.colour) {
      query.colour = colourValue;
    }

    if (controlChange.code) {
      query.code = codeValue;
    }
    if (controlChange.brand) {
      query.brand = brandValue;
    }
    if (controlChange.model) {
      query.model = modelValue;
    }

    query.page = defaultValues.page;
    setQuery({ ...query });
  };

  const handleClear = () => {
    setCodeValue("");
    setModelValue("");
    setBrandValue("");
    setPriceValue(defaultPrice);
    setColourValue("");
    setMinStartDateValue(defaultDate[0]);
    setMaxStartDateValue(defaultDate[1]);
    setMinEndDateValue(defaultDate[0]);
    setMaxEndDateValue(defaultDate[1]);

    controlChange = {
      code: false,
      model: false,
      brand: false,
      price: false,
      startDate: false,
      endDate: false,
      colour: false
    };
    setQuery({ ...defaultValues });
  };

  const  handleBrandChange = event => {
    controlChange.brand = true;
    setBrandValue(event.target.value);
  };

  const   handleModelChange = event => {
    controlChange.model = true;
    setModelValue(event.target.value);
  };

  const  handlePriceChange = (event, newValue) => {
    controlChange.price = true;
    setPriceValue(newValue);
  };

  const  handleCodeChange = event => {
    controlChange.code = true;
    setCodeValue(event.target.value);
  };
  const   handleColourChange = event => {
    controlChange.colour = true;
    setColourValue(event.target.value);
  };

  const  handleMinStartDateChange = event => {
    controlChange.startDate = true;
    setMinStartDateValue(event.target.value);
  };

  const   handleMaxStartDateChange = event => {
    controlChange.startDate = true;
    setMaxStartDateValue(event.target.value);
  };

  const   handleMinEndDateChange = event => {
    controlChange.endDate = true;
    setMinEndDateValue(event.target.value);
  };

  const   handleMaxEndDateChange = event => {
    controlChange.endDate = true;
    setMaxEndDateValue(event.target.value);
  };

  function  textPrice(value) {
    return `R$${value}°C`;
  }

  const marksPrice = [
    {
      value: 0,
      label: "0"
    },
    {
      value: 2500,
      label: "2500"
    },
    {
      value: 5000,
      label: "5000"
    },
    {
      value: 7500,
      label: "7500"
    },
    {
      value: 10000,
      label: "10000+"
    }
  ];

  return (
    <div>
      <form className={classes.container} noValidate autoComplete="off">
        <TextField
          value={codeValue}
          className={classes.textField}
          onChange={handleCodeChange}
          label="Code"
          margin="normal"
        />
        <TextField
          value={modelValue}
          className={classes.textField}
          onChange={handleModelChange}
          label="Model"
          margin="normal"
        />
        <TextField
          value={brandValue}
          className={classes.textField}
          onChange={handleBrandChange}
          label="Brand"
          margin="normal"
        />

        <FormControl className={clsx(classes.formControl, classes.textField)}>
          <Typography id="range-slider">Price Range:</Typography>

          <Slider
            getAriaValueText={textPrice}
            value={priceValue}
            onChange={handlePriceChange}
            valueLabelDisplay="auto"
            max={10000}
            min={0}
            aria-labelledby="range-slider"
            marks={marksPrice}
          />
        </FormControl>

        <FormControl className={clsx(classes.formControl, classes.textField)}>
          <Typography  color="primary" id="range-slider">Start Date Range:</Typography>
          <TextField
            id="standard-name"
            type="date"
            value={minStartDateValue}
            className={classes.textField}
            label="Min Date"
            margin="dense"
            onChange={handleMinStartDateChange}
          />

          <TextField
            id="standard-name"
            type="date"
            value={maxStartDateValue}
            className={classes.textField}
            label="Max Date"
            margin="dense"
            onChange={handleMaxStartDateChange}
          />
        </FormControl>

        <FormControl className={clsx(classes.formControl, classes.textField)}>
        <Typography color="secondary" id="range-slider">End Date Range:</Typography>
          <TextField
            color="secondary"
            id="standard-name"
            type="date"
            value={minEndDateValue}
            className={classes.textField}
            label="Min Date"
            margin="dense"
            onChange={handleMinEndDateChange}
          />

          <TextField
            id="standard-name"
            type="date"
            value={maxEndDateValue}
            className={classes.textField}
            label="Max Date"
            margin="dense"
            onChange={handleMaxEndDateChange}
          />
        </FormControl>

        <FormControl className={clsx(classes.formControl, classes.textField)}>
          <InputLabel htmlFor="colour">Colour</InputLabel>
          <Select
            name="colour"
            value={colourValue}
            onChange={handleColourChange}
          >
            <MenuItem value="WHITE">WHITE</MenuItem>
            <MenuItem value="BLACK">BLACK</MenuItem>
            <MenuItem value="GOLD">GOLD</MenuItem>
            <MenuItem value="PINK">PINK</MenuItem>
          </Select>
        </FormControl>
        

        <FormControl className={clsx(classes.formControl, classes.textField)}>
          <IconButton aria-label="Delete" onClick={handleClear}>
            <DeleteIcon />
          </IconButton>
          <Button variant="contained" color="primary" onClick={handleConfirm}>
            Search
          </Button>
        </FormControl>
      </form>
    </div>
  );
};
