import React, { useState, createContext } from "react";

export const QueryPhoneContext = createContext("QueryPhone");
export const PhoneTotalContext = createContext("PhoneCount");
export const PhoneResultContext = createContext("PhoneResult");

export const queryParams = {
  page: 1,
  code: undefined,
  model: undefined,
  minPrice: undefined,
  maxPrice: undefined,
  brand: undefined,
  minStartDate: undefined,
  maxStartDate: undefined,
  minEndDate: undefined,
  maxEndDate: undefined,
  colour: undefined
};


export const Context = ({ children }) => {

  const queryInitial = {...queryParams};
  

  const [result, setResult] = useState([]);
  const [query, setQuery] = useState(queryInitial);
  const [count, setCount] = useState(0);

  return (
    <div>
      <PhoneResultContext.Provider value={[result, setResult]}>
        <QueryPhoneContext.Provider value={[query, setQuery]}>
          <PhoneTotalContext.Provider value={[count, setCount]}>
            {children}
          </PhoneTotalContext.Provider>
        </QueryPhoneContext.Provider>
      </PhoneResultContext.Provider>
    </div>
  );
};
