import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  await app.listen(4000);
  Logger.log(`Rodando em: http://localhost:4000`, "Bootstrap da aplicação")
}
bootstrap();
