import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { TypeOrmModule } from '@nestjs/typeorm';

const config: TypeOrmModuleOptions = {
    type: 'sqlite',
    database: 'db_phone_management.db',
    entities: [ __dirname + '/../**/*.entity{.ts,.js}'],
    synchronize: true,
    logging: true,
};

export const TypeConfigORM = TypeOrmModule.forRoot(config);
