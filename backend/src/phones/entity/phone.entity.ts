import { Entity, BaseEntity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { PhonesColours } from '../enum/colour-enum';

@Entity()
export class Phone extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        unique: true,
        length: 8,
    })
    code: string;

    @Column({
        length: 255,
    })
    model: string;

    @Column()
    price: number;

    @Column({
        length: 255,
    })
    brand: string;

    @Column({
        length: 255,
    })
    photo: string;

    @Column()
    startDate: Date;

    @Column()
    endDate: Date;

    @Column()
    colour: PhonesColours;

}
