import { Injectable, NotFoundException } from '@nestjs/common';
import { PhoneRepository } from './repository/phone.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { CreatePhoneDTO } from './dto/CreatePhone.dto';
import { Phone } from './entity/phone.entity';
import { DeleteResult } from 'typeorm';
import { GetPhoneFilterDTO } from './dto/FilterPhone.dto';
import { GetPhonesDTO } from './dto/GetPhones.dto';
import { UpdatePhoneDTO } from './dto/UpdatePhone.dto';

@Injectable()
export class PhonesService {

    constructor(
        @InjectRepository(PhoneRepository)
        private phoneRepository: PhoneRepository,
    ) { }

    async createPhone(createPhoneDTO: CreatePhoneDTO): Promise<Phone> {

        return this.phoneRepository.createPhone(createPhoneDTO);
    }

    async updatePhone(code: string, updatePhoneDTO: UpdatePhoneDTO): Promise<Phone> {

        return this.phoneRepository.updatePhone(code, updatePhoneDTO);

    }

    async getPhoneByCode(code: string): Promise<Phone> {

        const found = await this.phoneRepository.findOne({ code });
        if (!found) {
            throw new NotFoundException(`Phone Code: ${code}`);
        }
        return found;

    }

    async getPhones(getPhoneFilterDTO: GetPhoneFilterDTO): Promise<GetPhonesDTO> {
        return await this.phoneRepository.getTasks(getPhoneFilterDTO);
    }

    async deletePhone(code: string): Promise<DeleteResult> {

        /// SQLite nao suporta o affected.
        /// Metodos alternativos, pouco ortodoxo.
        await this.getPhoneByCode(code);
        const result = await this.phoneRepository.delete({ code });

        /*         if (result.affected === 0) {
                    throw new NotFoundException(`Phone ID: ${code}`);
                } */
        return result;
    }

}
