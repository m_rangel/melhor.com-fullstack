import { Module } from '@nestjs/common';
import { PhonesController } from './phones.controller';
import { PhonesService } from './phones.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PhoneRepository } from './repository/phone.repository';

@Module({
    imports: [TypeOrmModule.forFeature([PhoneRepository])],
    controllers: [PhonesController],
    providers: [PhonesService]
  })
export class PhonesModule {}
