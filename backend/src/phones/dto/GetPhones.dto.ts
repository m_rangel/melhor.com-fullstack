import { Phone } from "../entity/phone.entity";

export class GetPhonesDTO{

    count: number;
    phones: Phone[];

}
