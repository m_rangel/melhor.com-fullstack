import { IsNotEmpty, IsPositive, MinLength, MaxLength, IsDateString, IsEnum, Length, IsOptional } from 'class-validator';
import { PhonesColours } from '../enum/colour-enum';

export class UpdatePhoneDTO {

    @IsOptional()
    @IsNotEmpty()
    @MinLength(2)
    @MaxLength(255)
    model: string;

    @IsOptional()
    @IsNotEmpty()
    @IsPositive()
    price: number;

    @IsOptional()
    @IsNotEmpty()
    @MinLength(2)
    @MaxLength(255)
    brand: string;

    @IsOptional()
    @IsNotEmpty()
    @MinLength(2)
    @MaxLength(255)
    photo: string;

    @IsOptional()
    @IsNotEmpty()
    @IsDateString()
    startDate: Date;

    @IsOptional()
    @IsNotEmpty()
    @IsDateString()
    endDate: Date;

    @IsOptional()
    @IsNotEmpty()
    @IsEnum(PhonesColours, {
        message: 'colour must be: "BLACK, WHITE, GOLD, PINK"',
    })
    colour: PhonesColours;

}
