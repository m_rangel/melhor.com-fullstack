import { IsOptional, IsNotEmpty, IsEnum } from 'class-validator';
import { PhonesColours } from '../enum/colour-enum';

export class GetPhoneFilterDTO{

    @IsOptional()
    @IsNotEmpty()
    page: number;

    @IsOptional()
    @IsNotEmpty()
    code: string;

    @IsOptional()
    @IsNotEmpty()
    model: string;

    @IsOptional()
    @IsNotEmpty()
    minPrice: number;

    @IsOptional()
    @IsNotEmpty()
    maxPrice: number;

    @IsOptional()
    @IsNotEmpty()
    brand: string;

    @IsOptional()
    @IsNotEmpty()
    minStartDate: string;

    @IsOptional()
    @IsNotEmpty()
    maxStartDate: string;

    @IsOptional()
    @IsNotEmpty()
    minEndDate: string;

    @IsOptional()
    @IsNotEmpty()
    maxEndDate: string;

    @IsOptional()
    @IsEnum(PhonesColours, {
        message: 'colour must be: "BLACK, WHITE, GOLD, PINK"',
    })
    @IsNotEmpty()
    colour: PhonesColours;

}
