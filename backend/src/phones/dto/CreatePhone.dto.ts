import { IsNotEmpty, IsPositive, MinLength, MaxLength, IsDateString, IsEnum, Length } from 'class-validator';
import { PhonesColours } from '../enum/colour-enum';

export class CreatePhoneDTO {

    @IsNotEmpty()
    @Length(8, 8, {
        message: 'code must be equal to 8 characters',
    })
    code: string;

    @IsNotEmpty()
    @MinLength(2)
    @MaxLength(255)
    model: string;

    @IsNotEmpty()
    @IsPositive()
    price: number;

    @IsNotEmpty()
    @MinLength(2)
    @MaxLength(255)
    brand: string;

    @IsNotEmpty()
    @MinLength(2)
    @MaxLength(255)
    photo: string;

    @IsNotEmpty()
    @IsDateString()
    startDate: Date;

    @IsNotEmpty()
    @IsDateString()
    endDate: Date;

    @IsNotEmpty()
    @IsEnum(PhonesColours, {
        message: 'colour must be: "BLACK, WHITE, GOLD, PINK"',
    })
    colour: PhonesColours;

}
