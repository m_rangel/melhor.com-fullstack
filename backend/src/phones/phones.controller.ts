import { Controller, Get, Delete, Post, Put, Param, Body, UsePipes, ValidationPipe, Query } from '@nestjs/common';
import { PhonesService } from './phones.service';
import { CreatePhoneDTO } from './dto/CreatePhone.dto';
import { Phone } from './entity/phone.entity';
import { PhoneCondensePipe } from './pipe/PhoneCondense.pipe';
import { PhoneCheckDatesPipe } from './pipe/PhoneCheckDates.pipe';
import { DeleteResult } from 'typeorm';
import { GetPhoneFilterDTO } from './dto/FilterPhone.dto';
import { GetPhonesDTO } from './dto/GetPhones.dto';
import { UpdatePhoneDTO } from './dto/UpdatePhone.dto';

@Controller('phones')
export class PhonesController {

    constructor(private phonesService: PhonesService){}

    @Get()
    @UsePipes( new PhoneCondensePipe() )
    getPhones(@Query(ValidationPipe) getPhoneFilterDTO: GetPhoneFilterDTO): Promise<GetPhonesDTO> {
        return this.phonesService.getPhones(getPhoneFilterDTO);
    }

    @Get('/:code')
    getPhoneByCode(@Param('code') code: string): Promise<Phone> {
        return this.phonesService.getPhoneByCode(code);
    }

    @Delete('/:code')
    deletePhoneByCode(@Param('code') code: string): Promise<DeleteResult> {
        return this.phonesService.deletePhone(code);
    }

    @Post()
    @UsePipes( new PhoneCheckDatesPipe() )
    @UsePipes(ValidationPipe)
    @UsePipes( new PhoneCondensePipe() )
    createPhone(@Body() createPhoneDTO: CreatePhoneDTO ): Promise<Phone> {
        return this.phonesService.createPhone(createPhoneDTO);
    }

    @Put('/:code')
    @UsePipes(ValidationPipe)
    @UsePipes(new PhoneCondensePipe())
    updateTaskByCode(@Param('code') code: string, @Body() updatePhoneDTO: UpdatePhoneDTO): Promise<Phone> {
        return this.phonesService.updatePhone(code, updatePhoneDTO);
    }

}
