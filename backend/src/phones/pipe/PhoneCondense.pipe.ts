import { PipeTransform } from '@nestjs/common';
import { CreatePhoneDTO } from '../dto/CreatePhone.dto';

export class PhoneCondensePipe implements PipeTransform {

  transform(values: CreatePhoneDTO) {

    if (values.model) {
      values.model = values.model.split(' ').join('');
    }

    if (values.brand) {
      values.brand = values.brand.split(' ').join('');
    }

    if (values.photo) {
      values.photo = values.photo.split(' ').join('');
    }

    return values;
  }

}
