import { PipeTransform, BadRequestException } from '@nestjs/common';
import { CreatePhoneDTO } from '../dto/CreatePhone.dto';
import { Validator } from 'class-validator';


export class PhoneCheckDatesPipe implements PipeTransform {

  transform(values: CreatePhoneDTO) {

    const validator = new Validator();

    const minDate = new Date(2018, 11, 25);
    const startDate = new Date(values.startDate);
    const endDate = new Date(values.endDate);

    if(!validator.minDate(startDate, minDate)){
      throw new BadRequestException(`The start date: ${startDate.toLocaleString()} is earlier than the minimum date: ${minDate.toLocaleString()}.` );
    }

    if(!validator.minDate(endDate, startDate)){
      throw new BadRequestException(`The end date: ${endDate.toLocaleString()}  is earlier than the minimum start date: ${startDate.toLocaleString()}.` );
    }

    return values;
  }

}
