import { Repository, EntityRepository } from 'typeorm';
import { Phone } from '../entity/phone.entity';
import { CreatePhoneDTO } from '../dto/CreatePhone.dto';
import { BadRequestException, Query, NotFoundException } from '@nestjs/common';
import { GetPhoneFilterDTO } from '../dto/FilterPhone.dto';
import { GetPhonesDTO } from '../dto/GetPhones.dto';
import { UpdatePhoneDTO } from '../dto/UpdatePhone.dto';
import { Validator } from 'class-validator';

@EntityRepository(Phone)
export class PhoneRepository extends Repository<Phone> {

    async createPhone(createPhoneDTO: CreatePhoneDTO): Promise<Phone> {

        let save: Phone | PromiseLike<Phone>;

        const {
            code,
            model,
            price,
            brand,
            photo,
            startDate,
            endDate,
            colour,
        } = createPhoneDTO;

        const phone = new Phone();

        phone.code = code;
        phone.model = model;
        phone.price = price;
        phone.brand = brand;
        phone.photo = photo;
        phone.startDate = startDate;
        phone.endDate = endDate;
        phone.colour = colour;

        try {
            save = await phone.save();
        } catch (error) {

            throw new BadRequestException(error.message);

        }
        return save;
    }

    async getTasks(getPhoneFilterDTO: GetPhoneFilterDTO): Promise<GetPhonesDTO> {

        const { page, code, model,
            minPrice, maxPrice, brand,
            minStartDate, maxStartDate,
            minEndDate, maxEndDate,
            colour } = getPhoneFilterDTO;

        const query = this.createQueryBuilder('phone');
        const getPhones = new GetPhonesDTO();
        let queryStartDate: string;
        let queryEndDate: string;
        let queryMinPrice: number;
        let queryMaxPrice: number;

        if (code) {
            // Permitir busca parcial;   ????
            query.andWhere('phone.code LIKE :code', { code: `%${code}%` });
        }

        if (model) {
            query.andWhere('phone.model = :model', { model });
        }

        if (brand) {
            query.andWhere('phone.brand = :brand', { brand });
        }

        if (colour) {
            query.andWhere('phone.colour = :colour', { colour });
        }

        if (minStartDate || maxStartDate) {
            queryStartDate = this.fixStartDate(minStartDate);
            queryEndDate = this.fixEndDate(maxStartDate);
            query.andWhere('phone.startDate BETWEEN :queryStartDate AND :queryEndDate', { queryStartDate, queryEndDate });
        }

        if (minEndDate || maxEndDate) {
            queryStartDate = this.fixStartDate(minEndDate);
            queryEndDate = this.fixEndDate(maxEndDate);
            query.andWhere('phone.endDate BETWEEN :queryStartDate AND :queryEndDate', { queryStartDate, queryEndDate });
        }

        if (minPrice || maxPrice) {
            queryMinPrice = minPrice || 0;
            queryMaxPrice = maxPrice || Infinity;
            query.andWhere('phone.price BETWEEN :queryMinPrice AND :queryMaxPrice', { queryMinPrice, queryMaxPrice });
        }

        getPhones.count = await query.getCount();

        if (page) {
            query.take(5);
            query.skip(5 * (page - 1));
        }

        getPhones.phones = await query.getMany();

        return getPhones;

    }

    async updatePhone(code: string, updatePhoneDTO: UpdatePhoneDTO): Promise<Phone> {

        const found = await this.findOne({ code });

        if (!found) {
            throw new NotFoundException(`Phone Code: ${code}`);
        }

        if (updatePhoneDTO.endDate || updatePhoneDTO.startDate) {

            const startDate = new Date(updatePhoneDTO.startDate);
            const endDate = new Date(updatePhoneDTO.endDate);
            this.validateDates(endDate, startDate, found.endDate, found.startDate);
        }


        await this.update({ code }, updatePhoneDTO);
        return await this.findOne({ code });
    }

    private validateDates(endDate, startDate, foundEndDate, foundStartDate) {

        const validator = new Validator();
        const minDate = new Date(2018, 11, 25);

        if (startDate) {
            if (!validator.minDate(startDate, minDate)) {
                throw new BadRequestException(
                    `The start date: ${startDate.toLocaleString()} is earlier than the minimum date: ${minDate.toLocaleString()}.`,
                );
            }

            if (!validator.minDate(startDate, foundEndDate) && !endDate) {
                throw new BadRequestException(
                    `The start date: ${startDate.toLocaleString()} is earlier than the minimum date: ${foundEndDate.toLocaleString()}.`,
                );
            }

        }

        if (endDate) {
            if (!validator.minDate(endDate, startDate || foundStartDate)) {
                throw new BadRequestException(
                    `The start date: ${startDate.toLocaleString()} is earlier than the minimum date: ${startDate.toLocaleString()}.`,
                );
            }
        }

    }

    private fixStartDate(stringDate: string) {

        let startDate: string;

        if (stringDate) {
            startDate = stringDate.replace(/([.|\[\]\/\\])/g, '-');
        } else {
            startDate = '2018-12-25';
        }
        return startDate;
    }
    private fixEndDate(stringDate: string) {

        let endDate: string;

        if (stringDate) {
            endDate = stringDate.replace(/([.|\[\]\/\\])/g, '-');
        } else {
            endDate = '2030-12-30';
        }
        return endDate;
    }

}
