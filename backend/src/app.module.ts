import { Module } from '@nestjs/common';
import { PhonesModule } from './phones/phones.module';
import { TypeConfigORM } from './database/typeorm.config';

@Module({
  imports:
  [
    TypeConfigORM,
    PhonesModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
