### Melhor Comunicão : Teste FullStack.
##### Mauro Rangel - mauro.rangel@outlook.com.br
------------
https://github.com/melhorcom/full-stack-teste

------------

####Proposta:
Uma empresa vai lançar um novo app para venda de celulares e com isso gostaríamos que você construísse uma API para gerenciar o cadastro de novos celulares.
####Descrição do problema:
A solução consistirá em criar uma API REST escrita em **NodeJS** e uma aplicação frontend usando React.

####Técnologias utilizadas no teste: 
#####Back-End:
[Nest JS - Framework Node JS ](https://nestjs.com/ "Nest JS - Framework Node JS ");
> Por ser mais enterprise grade, tem uma arquitetura bem definida e uma carinha de spring e .net.

[TypeORM com Driver de SQLITE;](https://typeorm.io/#/ "TypeORM com Driver de SQLITE;")
> Como exemplo de persistência irei utilizar typeorm com o sqlite como um exemplo para gravação e leitura dos dados em um banco de dados.

#####Front-End:
React;
Redux?
ApiSauce?

####Como executar a aplicação:
#####Back-End: 

------------

######No root da pasta back-end: 
npm install
npm run start:dev
HTTP:LOCALHOST:4000 - ROOT DA API.
------------

#####Front-End: 
######No root da pasta front-end: 
npm install
npm run start
HTTP:LOCALHOST:3000
------------
###CRUD:

#####POST:
uri
http://localhost:4000/phones/
body:
{
	"code" : "12345678",
    "model" : "",
    "price" : "",
    "brand" : "",
    "photo" : "",
    "startDate" :  "2018-012-25T18:25:43.511Z",
    "endDate" : "",
    "colour" : ""
}

#####GET ONE:
uri
http://localhost:3000/phones/:code

#####DELETE ONE:
uri
http://localhost:3000/phones/:code

#####GET MANY:
http://localhost:3000/phones
Filtros:
?minPrice=40000
?maxPrice=80000
?model="teste"
?code="teste"
?page=1
?brand="teste"
?minStartDate= "25/04/2012"
?maxStartDate = "25/04/2012"
?minEndDate= "25/04/2012"
?maxEndDate= "25/04/2012"

    @IsOptional()
    @IsNotEmpty()
    page: number;

    @IsOptional()
    @IsNotEmpty()
    code: string;

    @IsOptional()
    @IsNotEmpty()
    model: string;

    @IsOptional()
    @IsNotEmpty()
    minPrice: number;

    @IsOptional()
    @IsNotEmpty()
    maxPrice: number;

    @IsOptional()
    @IsNotEmpty()
    brand: string;

    @IsOptional()
    @IsNotEmpty()
    minStartDate: string;

    @IsOptional()
    @IsNotEmpty()
    maxStartDate: string;

    @IsOptional()
    @IsNotEmpty()
    minEndDate: string;

    @IsOptional()
    @IsNotEmpty()
    maxEndDate: string;

    @IsOptional()
    @IsEnum(PhonesColours, {
        message: 'colour must be: "BLACK, WHITE, GOLD, PINK"',
    })
    @IsNotEmpty()
    colour: PhonesColours;


#####PUT:
uri
http://localhost:3000/phones/:code
{
	"model" : "",
    "price" : "",
    "brand" : "",
    "photo" : "",
    "startDate" :  "2018-012-25T18:25:43.511Z",
    "endDate" : "",
    "colour" : ""
}


